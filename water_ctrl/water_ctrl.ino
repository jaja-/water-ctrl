#define FLOW_INCREMENT 0.00058
#define PRESSURE_OFFSET 0.498
#define PRESSURE_RATIO 75/2.25
#define MAX_PUMP_PRESSURE 80
#define TICKER_MAX 10
#define PHANTOM_FLOW_DELAY 3000
#define ITER_DELAY 500

volatile double inputflow;
volatile byte ticker;
volatile boolean button;
volatile float pumppressure;
volatile boolean pumprelay;

// UI Functions

void flash_led() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(250);
  digitalWrite(LED_BUILTIN, LOW);
  delay(250);
}

void acknowledge() {
  for(int i = 0; i < 3; i++) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(50);
    digitalWrite(LED_BUILTIN, LOW);
    delay(50);
  }
  delay(2000);
}

void status_wait(byte pulses) {
  byte iters = 0;
  relay_state(0);
  while(true) {
    for(byte i = 0; i < pulses; i++)
      flash_led();
    if(digitalRead(7) == 1) {
      acknowledge();
      break;
    }
    else if(iters > 0)
      break;
    delay(500);
  }
  if(iters > 0)
    relay_state(1);
  delay(1000);  // Wait a second to let button be released
}

void emergency_off_check() {
  if(digitalRead(7) == 1)
    status_wait(10);
}

void (*resetFunc)(void) = 0;

void inputflow_pulse() {
  inputflow += FLOW_INCREMENT;
  ticker = (ticker <= TICKER_MAX) ? ticker + 1 : TICKER_MAX;
}

// Each line will output as follows:
// if|tk|bt|pp|pr
void send_data() {
  Serial.print(inputflow);
  Serial.print('|');
  Serial.print(ticker);
  Serial.print('|');
  Serial.print(button);
  Serial.print('|');
  Serial.print(pumppressure);
  Serial.print('|');
  Serial.println(pumprelay);
}

void relay_state(boolean state) {
  if(state && !pumprelay)
    digitalWrite(6, HIGH);
  else if(!state && pumprelay)
    digitalWrite(6, LOW);
  pumprelay = digitalRead(6);
}

void pressure_check() {
  pumppressure = ((analogRead(1) * 5.0 / 1024) - PRESSURE_OFFSET) * PRESSURE_RATIO;
  
  // Stop if input pressure too high
  while(pumppressure > MAX_PUMP_PRESSURE) {
    status_wait(1);
    delay(1000);
  }
}

void ticker_expire() {
  ticker = (ticker > 0) ? ticker-1 : 0;
  relay_state(ticker);
  if(ticker < 1)
    delay(PHANTOM_FLOW_DELAY);
    ticker = 0;
}

void setup() {
  button = 0;
  pumppressure = 0.0;
  pumprelay = 0;
  inputflow = 0.0;
  
  pinMode(LED_BUILTIN, OUTPUT);
  
  attachInterrupt(0, inputflow_pulse, RISING);  //DIGITAL Pin 2: Interrupt 0
  
  pinMode(6, OUTPUT);  // Pump Relay
  pinMode(7, INPUT);   // Acknowledge Button
    
  // Analog1: Pump pressure sensor
  
  Serial.begin(9600);
  
  relay_state(1);
  delay(500);
}

void loop() {
  emergency_off_check();
  pressure_check();
  ticker_expire();
  send_data();
  delay(ITER_DELAY);
}
