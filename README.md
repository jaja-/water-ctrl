# Water Filter Controller
---
This project was developed from a need to more precisely control
the a water filter system. The original booster pump switch that
came with the kit did not seem to funtion, and tuning the 
booster pump to a both optimal and safe operating range was
fraught with too much guess-work. As such, this project uses an
Arduino to help facilitate proper funtioning with the use of
water pressure and flow sensors.

All of the code, loggers, documentation, and scripts are written
in the Jupyter Lab notebooks. For more information, please
explore the documentation within the Jupyter notebooks!
